package com.potugi.datagenerate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "currencypairs")
public class CurrencyPairs {
    private String currentName;
    private double value;
    private String timeStamp;
}

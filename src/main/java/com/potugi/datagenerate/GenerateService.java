package com.potugi.datagenerate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.concurrent.CompletableFuture;

@Service
public class GenerateService {

    @Autowired
    private final DataServiceCommon data;
    public GenerateService(DataServiceCommon data) {
        this.data = data;
    }
    public CompletableFuture<Iterable<CurrencyPairs>> getCurrencyPairs(String namePairs) throws InterruptedException {
        return data.generateCurrencyPairs(namePairs);
    }
}

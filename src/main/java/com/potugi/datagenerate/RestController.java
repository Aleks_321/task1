package com.potugi.datagenerate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@org.springframework.web.bind.annotation.RestController

@Controller
public class RestController {
    private final GenerateService data;

    RestController(GenerateService data) {
        this.data = data;
    }

    @ResponseBody
    @GetMapping("/curencypairs/{pairName}")
    public CompletableFuture<Iterable<CurrencyPairs>>
    getCurrencyPairs(@PathVariable String pairName) throws InterruptedException {
        return data.getCurrencyPairs(pairName);
    }
}

package com.potugi.datagenerate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;


@SpringBootApplication
@ConfigurationPropertiesScan
public class DatagenerateApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatagenerateApplication.class, args);
    }

}

package com.potugi.datagenerate;

import org.apache.commons.math3.util.Precision;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
@Component
public class DataServiceCommon {
    private final CurrencyPairs pairs;
    private final Random rand = new Random();

    public DataServiceCommon(CurrencyPairs pairs) {
        this.pairs = pairs;
    }

    @Async
    public CompletableFuture<Iterable<CurrencyPairs>> generateCurrencyPairs(String namePairs) throws InterruptedException {
        List<CurrencyPairs> con = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            con.add(new CurrencyPairs(new String(namePairs)
                    , Precision.round(rand.nextDouble(), 3)
                    , Instant.now().toString()));
        }
        for (CurrencyPairs it : con) {
            System.out.println(it.toString());
        }
        return CompletableFuture.completedFuture(con);
    }
}

